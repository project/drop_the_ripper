<?php

namespace Drush\Commands;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * A Drush commandfile.
 */
class DropTheRipperCommands extends DrushCommands {

  /**
     * Default batch size.
     *
     * @var int
     */
  const DTR_BATCH_SIZE = 25000;

  /**
   * Path to a custom wordlist, or 'default'.
   *
   * @var string
   */
  protected $wordlist;

  /**
   * Number of entries from the wordlist to use.
   *
   * @var int
   */
  protected $top;

  /**
   * Whether to use the entire wordlist.
   *
   * @var bool
   */
  protected $all;

  /**
   * Whether to hide cracked passwords.
   *
   * @var bool
   */
  protected $hide;

  /**
   * Crack Drupal password hashes.
   *
   * @param string $user_rids
   *   (Optional) Only check passwords for users with these role IDs (comma
   *   separate multiple IDs).
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option wordlist
   *   Path to a custom wordlist (default is openwall's password list).
   * @option top
   *   Number of passwords to read from the wordlist (default is 25).
   * @option all
   *   Use all entries from the wordlist (default if a custom wordlist is
   * supplied).
   * @option hide
   *   Do not show plaintext passwords in output.
   * @option uid
   *   Comma separated list of user ids.
   * @option restricted
   *   Check all users with roles that have restricted (admin) permissions.
   * @option no-guessing
   *   Disables built-in password guessing (e.g. username as password).
   * @option batch-size
   *   Number of users to process in each batch (default is 25000). Try reducing
   * this if memory is exhausted.
   * @bootstrap full
   * @usage drush dtr
   *   Try to crack passwords of all users.
   * @usage drush dtr --top=100
   *   Try to crack passwords of all users, using the first 100 passwords from
   * the wordlist.
   * @usage drush dtr 3
   *   Try to crack passwords of all users with role 3 in D7 ("drush rls" lists
   * role IDs).
   * @usage drush dtr editor
   *   Try to crack passwords of all users with editor role in D8 ("drush rls"
   * lists role IDs).
   * @usage drush dtr --uid=1
   *   Try to crack password of user number 1.
   * @usage drush dtr --restricted
   *   Try to crack passwords of all users with roles that have restricted
   * permissions.
   * @usage drush dtr --wordlist=/tmp/rockyou.txt
   *   Use a custom wordlist for password cracking.
   * @usage drush dtr --all --no-guessing
   *   Try every password in the wordlist, but do not try to guess user
   * passwords.
   *
   * @command drop:the-ripper
   * @aliases dtr,drop-the-ripper
   */
  public function theRipper($user_rids = 'all', array $options = [
    'wordlist' => 'default',
    'top' => 25,
    'all' => NULL,
    'hide' => FALSE,
    'uid' => 'all',
    'restricted' => FALSE,
    'no-guessing' => FALSE,
    'batch-size' => self::DTR_BATCH_SIZE,
  ]) {
    $start = microtime(TRUE);

    $this->wordlist = $options['wordlist'];
    $this->all = $options['all'];
    $this->top = $options['top'];
    $this->hide = $options['hide'];

    $batchSize = $this->getBatchSize($options['batch-size']);
    $try_guesses = !$options['no-guessing'];
    $user_uids = $options['uid'];
    $user_checks = 0;
    $pw_checks = 0;

    $wordlist = $this->getWordlist();
    if (!$wordlist) {
      $msg = 'Could not find that wordlist.';
      throw new \Exception($msg);
    }
    $passwords = $this->loadWordlist($wordlist);

    $user_query = \Drupal::database()->select('users_field_data', 'u')
      ->fields('u')
      ->condition('u.uid', 0, '>');
    $user_query->distinct();
    $conditions = new \Drupal\Core\Database\Query\Condition('OR');

    $uids = [];
    if ($options['restricted']) {
      // Ensure User 1 is included as having restricted perms.
      $uids += [1];
      // This argument overrides any supplied rids.
      $user_rids = implode(',', $this->getRestrictedRoles());
    }
    if ($user_uids != 'all' && $uid_opt = $this->getIds($user_uids)) {
      $uids += $uid_opt;
    }
    $uids = array_unique($uids);
    if (count($uids)) {
      $conditions->condition('u.uid', $uids, 'IN');
    }

    if ($user_rids != 'all') {
      if ($rids = $this->getRoleTargetIds($user_rids)) {
        $user_query->leftJoin(
        'user__roles',
        'ur',
        'u.uid = ur.entity_id AND ur.bundle = :user',
        [':user' => 'user']
        );
        $conditions->condition('ur.roles_target_id', $rids, 'IN');
      }
    }

    if ($conditions->count()) {
      $user_query->condition($conditions);
    }

    $num_users = $user_query->countQuery()->execute()->fetchField();
    $this->logger()->notice(dt('Users to check: @num', array(
      '@num' => $num_users,
    )));
    $loops = ceil($num_users / $batchSize);
    $this->logger()->debug(dt('Running in batches: users=@num_users, batch_size=@batch_size, loops=@loops', [
      '@num_users' => $num_users,
      '@batch_size' => $batchSize,
      '@loops' => $loops,
    ]));
    for ($index = 0; $index < $loops; $index++) {
      $offset = (int) $index * $batchSize;
      $this->logger()->debug(dt('Batch run: index=@index, offset=@offset, limit=@limit', [
        '@index' => $index,
        '@offset' => $offset,
        '@limit' => $batchSize,
      ]));

      $user_query->range($offset, $batchSize)->orderBy('uid', 'ASC');
      $users = $user_query->execute()
        ->fetchAll();

      foreach ($users as $user) {
        $user_checks++;
        if ($try_guesses) {
          $guesses = $this->userGuesses($user);
          foreach ($guesses as $guess) {
            $pw_checks++;
            if ($this->checkPassword($user, $guess)) {
              // No need to try the passwords for this user.
              continue 2;
            }
          }
        }
        foreach ($passwords as $password) {
          $pw_checks++;
          if ($this->checkPassword($user, $password)) {
            break;
          }
        }
      }
    }
    $finish = microtime(TRUE);
    $this->logger()->success(dt('Ran @pc password checks for @uc users in @sec seconds.', [
      '@pc' => $pw_checks,
      '@uc' => $user_checks,
      '@sec' => sprintf('%.2f', $finish - $start),
    ]));
  }

  /**
   * Check a user password in Drupal 8.
   *
   * @param object $user
   *   A (minimal) user object.
   * @param string $password
   *   The password to check.
   *
   * @return bool
   *   Whether the password matched.
   */
  public function checkPassword($user, $password) {
    $password_checker = \Drupal::service('password');
    $matched = FALSE;
    // Disabled for now as DEBUG_NOTIFY no longer seems to only show up when
    // running with both debug and verbose; as just DEBUG this is too noisy.
    /*
    $msg = "Check: uid=@uid name=@name for password '@pass'";
    $this->logger()->log(LogLevel::DEBUG_NOTIFY, dt($msg, [
    '@uid' => $user->uid,
    '@name' => $user->name,
    '@pass' => $password,
    ]));
     */
    if ($password_checker->check($password, $user->pass)) {
      $this->logger()->notice(dt('Match: uid=@uid name=@name status=@status password=@pass', [
        '@uid' => $user->uid,
        '@name' => $user->name,
        '@status' => $user->status,
        '@pass' => $this->hide ? 'XXXXXXXXXX' : $password,
      ]));
      $matched = TRUE;
    }
    return $matched;
  }

  /**
   * Get a list of restricted permissions in Drupal 8.
   *
   * @return array
   *   The names of permissions marked as restricted.
   */
  public function getRestrictedPermissions() {
    $all_perms = \Drupal::service('user.permissions')->getPermissions();
    foreach ($all_perms as $name => $perm) {
      if (!empty($perm['restrict access'])) {
        $restricted_perms[] = $name;
      }
    }
    $this->logger()->debug(dt('Restricted perms: @perms', ['@perms' => implode(', ', $restricted_perms)]));
    return $restricted_perms;
  }

  /**
   * Get a list of roles with restricted permissions in Drupal 8.
   *
   * @return array
   *   The names of roles with restricted permissions.
   */
  public function getRestrictedRoles() {
    $restricted_perms = $this->getRestrictedPermissions();
    $roleStorage = \Drupal::service('entity_type.manager')->getStorage('user_role');
    $roles = $roleStorage->loadMultiple();
    $restricted_roles = array();
    foreach ($roles as $name => $role) {
      if ($role->isAdmin() || count(array_intersect($restricted_perms, $role->getPermissions()))) {
        $restricted_roles[] = $name;
      }
    }
    $this->logger()->debug(dt('Restricted roles: @roles', ['@roles' => implode(', ', $restricted_roles)]));
    return $restricted_roles;
  }

  /**
   * Process the --wordlist option, or use a default.
   *
   * @return string|bool
   *   Path to the wordlist, or FALSE if it does not exist.
   */
  public function getWordlist() {
    $wordlist = $this->wordlist;
    if ($wordlist == 'default') {
      $wordlist = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'password';
    }
    else {
      // Custom wordlist; use all entries unless a --top option was supplied.
      if ((int) $this->top == 0) {
        $this->all = TRUE;
      }
    }
    if (!file_exists($wordlist)) {
      $wordlist = FALSE;
    }
    return $wordlist;
  }

  /**
   * Make a few guesses about a user's password.
   *
   * @param object $user
   *   A (basic) user object.
   *
   * @return array
   *   Guesses at the user's password.
   */
  public function userGuesses($user) {
    $guesses = array();
    $guesses[] = $user->name;
    $guesses[] = strtolower($user->name);
    $guesses[] = strtoupper($user->name);

    // @see https://www.drupal.org/project/drop_the_ripper/issues/3219538
    $suffixes = array(
      '1',
      '123',
      '!',
      date('Y'),
      date('Y') - 1,
    );
    foreach ($suffixes as $suffix) {
      $guesses[] = $user->name . $suffix;
    }

    $guesses[] = $user->mail;
    if (preg_match('/(.*)@(.*)\..*/', $user->mail, $matches)) {
      // Username portion of mail.
      $guesses[] = $matches[1];
      // First part of domain.
      $guesses[] = $matches[2];
    }
    return array_unique(array_filter($guesses));
  }

  /**
   * Parse a wordlist file into an array.
   *
   * @param string $wordlist
   *   Path to the wordlist file.
   *
   * @return array
   *   Candidate passwords.
   */
  public function loadWordlist($wordlist) {
    $passwords = file($wordlist);
    $passwords = array_filter($passwords, [$this, 'wordlistFilterCallback']);
    $passwords = array_map([$this, 'trimNewline'], $passwords);
    $passwords = array_unique($passwords);

    if (!$this->all) {
      if (($top = (int) $this->top) > 0) {
        $passwords = array_slice($passwords, 0, $top);
      }
    }

    return $passwords;
  }

  /**
   * Callback for wordlist array filtering; removes comments.
   *
   * @param string $line
   *   An item from a wordlist.
   *
   * @return bool
   *   FALSE if the line is a comment.
   */
  public function wordlistFilterCallback($line) {
    return (strpos($line, '#!comment:') !== 0);
  }

  /**
   * Callback for wordlist array trimming; remove only trailing newlines.
   *
   * @param string $line
   *   An item from a wordlist.
   *
   * @return string
   *   Candidate password with trailing newline removed.
   */
  public function trimNewline($line) {
    // Note that double quotes are necessary for the whitespace characters.
    return rtrim($line, "\r\n");
  }

  /**
   * Parse the supplied (u|r)ids option.
   *
   * @param string $ids
   *   The user-supplied list of uids or rids.
   *
   * @return array|bool
   *   Array of numeric ids, or FALSE if none were valid.
   */
  public function getIds($ids) {
    $ids = explode(',', $ids);
    $ids = array_map('trim', $ids);
    $ids = array_filter($ids, 'is_numeric');
    return (empty($ids) ? FALSE : $ids);
  }

  /**
   * Parse the supplied roles option.
   *
   * @param string $user_rids
   *   The user-supplied list of roles.
   *
   * @return array|bool
   *   Array of roles_target_id's, or FALSE if none were valid.
   */
  public function getRoleTargetIds($user_rids) {
    $rids = explode(',', $user_rids);
    $rids = array_map('trim', $rids);
    // @todo: filter target_id (based on ASCII character set)
    return (empty($rids) ? FALSE : $rids);
  }

  /**
   * Get the batch size for this run.
   *
   * @return int
   *   The batch size that should be used.
   */
  public function getBatchSize($option) {
    if ($option != self::DTR_BATCH_SIZE) {
      return (int) $option;
    }
    // @todo: dynamically choose a batch size based on available memory.
    $batch_size = self::DTR_BATCH_SIZE;
    return $batch_size;
  }

  /**
   * Generate a report for D8/9.
   *
   * @command drop:the-report
   * @aliases dtrr,drop-the-report
   * @bootstrap full
   */
  public function theReport(OutputInterface $output) {
    $old_verbosity = $output->getVerbosity();
    $output->setVerbosity(OutputInterface::VERBOSITY_DEBUG);

    $user_query = \Drupal::database()->select('users_field_data', 'u')
      ->fields('u')
      ->condition('u.uid', 0, '>');
    $user_query->distinct();
    $conditions = new \Drupal\Core\Database\Query\Condition('OR');

    // Ensure User 1 is included as having restricted perms.
    $uids = array(1);
    $conditions->condition('u.uid', $uids, 'IN');

    $roles = $this->getRestrictedRoles();
    $user_rids = implode(',', $roles);
    if ($rids = $this->getRoleTargetIds($user_rids)) {
      $user_query->leftJoin(
        'user__roles',
        'ur',
        'u.uid = ur.entity_id AND ur.bundle = :user',
        array(':user' => 'user')
      );
      $conditions->condition('ur.roles_target_id', $rids, 'IN');
    }
    $user_query->condition($conditions);
    $users = $user_query->execute()->fetchCol();

    $this->logger()->debug(dt('Restricted users: @users',
      ['@users' => implode(',', $users)]));

    $output->setVerbosity($old_verbosity);

    $this->logger()->success(dt('Command to list *all* roles: drush role-list'));
    $this->logger()->success(dt('Command to see permissions assigned to a role: drush role-list @role',
      ['@role' => array_pop( $roles)]));
    $this->logger()->success(dt('Command to list users with one or more restricted permissions: drush user-information --uid=@users',
      ['@users' => implode(',', $users)]));
  }

}
