<?php

/**
 * Hacky script to set up a test environment for Drop the Ripper.
 *
 * Run this using drush scr
 */

const EOL = PHP_EOL;

print "This script runs a site-install and then sets up some test" . EOL;
print "users and roles which can be used to test Drop the Ripper." . EOL;
print "THIS WILL DELETE YOUR SITE DATABASE WITHOUT TAKING A BACKUP!!!1!" . EOL;
print "Are you sure you want to continue? (type 'yes' to proceed)." . EOL;

$handle = fopen ("php://stdin","r");
$line = fgets($handle);
if(trim($line) != 'yes'){
  print "Okay, glad we checked!" . EOL;
  drush_exit_clean();
}
fclose($handle);
echo "Thank you, setting up test environment..." . EOL;

$test = new DropTheRipperHackyTest();
$test->drush('site-install', array(), array('account-name' => 'admin'));
$test->setUpDtrUsersAndRoles();

print "That's it. Happy testing!" . EOL;
print "P.S. don't leave a site set up like this on the internet!" . EOL;

/**
 * Clean Drush exit
 * https://jimconte.com/blog/web/exit-cleanly-from-a-custom-drush-command
 */
function drush_exit_clean() {
  drush_set_context('DRUSH_EXECUTION_COMPLETED', TRUE);
  drush_set_context('DRUSH_EXIT_CODE', DRUSH_SUCCESS);
  exit(0);
}

class DropTheRipperHackyTest {

  protected $siteOptions = array();

  public function drush($command, $args = array(), $options = array()) {
    $result = drush_invoke_process('@self', $command, $args, $options);
    if (drush_get_context('DRUSH_VERBOSE') || drush_get_context('DRUSH_DEBUG')) {
      print_r($result);
    }
  }

  // Copied from DropTheRipperTest
  
  /**
   * Create test users and roles.
   */
  public function setUpDtrUsersAndRoles() {
    // Create some users.
    $users = array(
      'arthur' => array(
    // Top 25.
        'password' => '12345',
      ),
      'ford' => array(
    // Top 25.
        'password' => 'abc123',
      ),
      'zaphod' => array(
    // Top 25.
        'password' => 'computer',
        'blocked'  => TRUE,
      ),
      'marvin' => array(
    // Top 50.
        'password' => 'changeme',
        'blocked'  => TRUE,
      ),
      'trillian' => array(
    // Not in wordlist.
        'password' => 'XkOjV?2#Mt3|7baT',
      ),
      'slartibartfast' => array(
    // Top 100.
        'password' => 'gandalf',
      ),
    );
    foreach ($users as $user => $account) {
      $this->drush('user-create', array($user), $this->siteOptions + array('password' => $account['password']));
      if (isset($account['blocked']) && !empty($account['blocked'])) {
        $this->drush('user-block', array($user), $this->siteOptions);
      }
    }
    // Also set user 1's password; "admin" is quite a long way down the
    // wordlist, but should be guessed based on the user name.
    $this->drush('user-password', array('admin'), $this->siteOptions + array('password' => 'admin'));

    // Create some custom roles
    // (n.b. there's a fairly limited set of permissions available)
    $roles = array(
      'earthman' => array(
        'perms' => array(
          'access content',
        ),
        'users' => array(
          'arthur',
          'trillian',
        ),
      ),
      'hitchhiker' => array(
        'perms' => array(
          'access user profiles',
          'change own username',
        ),
        'users' => array(
          'arthur',
          'ford',
          'trillian',
        ),
      ),
      'android' => array(
        'perms' => array(
    // Restricted.
          'administer software updates',
    // restricted.
          'select account cancellation method',
        ),
        'users' => array(
          'marvin',
        ),
      ),
      'designer' => array(
        'perms' => array(
    // Restricted.
          'administer site configuration',
          'administer themes',
        ),
        'users' => array(
          'slartibartfast',
        ),
      ),
    );
    foreach ($roles as $name => $role) {
      $this->drush('role-create', array($name), $this->siteOptions);
      $this->drush('role-add-perm', array($name, implode(',', $role['perms'])), $this->siteOptions);
      $this->drush('user-add-role', array($name, implode(',', $role['users'])), $this->siteOptions);
    }
  }
}
