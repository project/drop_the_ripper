<?php

namespace Unish;

/**
 * Tests for Drop the Ripper.
 *
 * @group commands
 * @group dtr
 */
class DropTheRipperTest extends CommandUnishTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    if (UNISH_DRUPAL_MAJOR_VERSION < 7) {
      $this->markTestSkipped('DtR supports D7 and D8');
    }
    if (!$this->getSites()) {
      $this->setUpDrupal(1, TRUE);
      $this->siteOptions = array(
        'root' => $this->webroot(),
        'uri' => key($this->getSites()),
        'yes' => NULL,
      );
      $this->setUpDtrUsersAndRoles();
    }
    else {
      $this->siteOptions = array(
        'root' => $this->webroot(),
        'uri' => key($this->getSites()),
        'yes' => NULL,
      );
    }
  }

  /**
   * Create test users and roles.
   */
  public function setUpDtrUsersAndRoles() {
    // Create some users.
    $users = array(
      'arthur' => array(
        // Top 25.
        'password' => '12345',
      ),
      'ford' => array(
        // Top 25.
        'password' => 'abc123',
      ),
      'zaphod' => array(
        // Top 25.
        'password' => 'computer',
        'blocked'  => TRUE,
      ),
      'marvin' => array(
        // Top 50.
        'password' => 'changeme',
        'blocked'  => TRUE,
      ),
      'trillian' => array(
        // Not in wordlist.
        'password' => 'XkOjV?2#Mt3|7baT',
      ),
      'slartibartfast' => array(
        // Top 100.
        'password' => 'gandalf',
      ),
      'nimda' => array(
        // Not in the wordlist, but should be guessed.
        'password' => 'nimda123',
      ),
    );
    foreach ($users as $user => $account) {
      $this->drush('user-create', array($user), $this->siteOptions + array('password' => $account['password']));
      if (isset($account['blocked']) && !empty($account['blocked'])) {
        $this->drush('user-block', array($user), $this->siteOptions);
      }
    }
    // Also set user 1's password; "admin" is quite a long way down the
    // wordlist, but should be guessed based on the user name.
    $this->drush('user-password', array('admin'), $this->siteOptions + array('password' => 'admin'));

    // Create some custom roles
    // (n.b. there's a fairly limited set of permissions available)
    $roles = array(
      'earthman' => array(
        'perms' => array(
          'access content',
        ),
        'users' => array(
          'arthur',
          'trillian',
        ),
      ),
      'hitchhiker' => array(
        'perms' => array(
          'access user profiles',
          'change own username',
        ),
        'users' => array(
          'arthur',
          'ford',
          'trillian',
        ),
      ),
      'android' => array(
        'perms' => array(
          // Restricted.
          'administer software updates',
          'select account cancellation method',
        ),
        'users' => array(
          'marvin',
          'nimda',
        ),
      ),
      'designer' => array(
        'perms' => array(
          // Restricted.
          'administer site configuration',
          'administer themes',
        ),
        'users' => array(
          'slartibartfast',
        ),
      ),
    );
    foreach ($roles as $name => $role) {
      $this->drush('role-create', array($name), $this->siteOptions);
      $this->drush('role-add-perm', array($name, implode(',', $role['perms'])), $this->siteOptions);
      $this->drush('user-add-role', array($name, implode(',', $role['users'])), $this->siteOptions);
    }
  }

  /**
   * Ensure that a log message does not appear in the Drush log.
   *
   * @param string $log
   *   Parsed log entries from backend invoke.
   * @param string $message
   *   The expected message that must be contained in
   *   some log entry's 'message' field.  Substrings will match.
   * @param string|bool $logType
   *   The type of log message to look for; all other
   *   types are ignored. If FALSE (the default), then all log types
   *   will be searched.
   */
  public function assertLogHasNotMessage($log, $message, $logType = FALSE) {
    foreach ($log as $entry) {
      if (!$logType || ($entry['type'] == $logType)) {
        if (strpos($entry['message'], $message) !== FALSE) {
          $this->fail("Found message in log: " . $message);
        }
      }
    }
    return TRUE;
  }

  /**
   * Test default options.
   */
  public function testDtrDefaultOptions() {
    $this->drush('drop-the-ripper', array(), $this->siteOptions + array('backend' => NULL));
    $parsed = $this->parse_backend_output($this->getOutput());
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=1 name=admin status=1 password=admin', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=2 name=arthur status=1 password=12345', 'success'));
    // This debug logging was removed because it ate memory.
    //$this->assertTrue($this->assertLogHasMessage($parsed['log'], "Check: uid=3 name=ford for password 'ford'", 'debug'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=3 name=ford status=1 password=abc123', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=4 name=zaphod status=0 password=computer', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=8 name=nimda status=1 password=nimda123', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=5 name=marvin', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=6 name=trillian', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=7 name=slartibartfast', 'success'));
  }

  /**
   * Test --top=X option.
   */
  public function testDtrTopOption() {
    $options = array('top' => 50);
    $this->drush('drop-the-ripper', array(), $this->siteOptions + array('backend' => NULL) + $options);
    $parsed = $this->parse_backend_output($this->getOutput());
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=1 name=admin status=1 password=admin', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=2 name=arthur status=1 password=12345', 'success'));
    // This debug logging was removed because it ate memory.
    //$this->assertTrue($this->assertLogHasMessage($parsed['log'], "Check: uid=3 name=ford for password 'ford'", 'debug'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=3 name=ford status=1 password=abc123', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=4 name=zaphod status=0 password=computer', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=5 name=marvin status=0 password=changeme', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=8 name=nimda status=1 password=nimda123', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=6 name=trillian', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=7 name=slartibartfast', 'success'));
  }

  /**
   * Test --restricted option.
   */
  public function testDtrRestrictedOption() {
    // @todo: should really only be testing one thing
    $options = array('restricted' => NULL, 'top' => 100);
    $this->drush('drop-the-ripper', array(), $this->siteOptions + array('backend' => NULL) + $options);
    $parsed = $this->parse_backend_output($this->getOutput());
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=1 name=admin status=1 password=admin', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=5 name=marvin status=0 password=changeme', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=7 name=slartibartfast status=1 password=gandalf', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=8 name=nimda status=1 password=nimda123', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=4 name=zaphod', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=6 name=trillian', 'success'));
  }

  /**
   * Test --hide option.
   */
  public function testDtrHideOption() {
    $options = array('hide' => NULL);
    $this->drush('drop-the-ripper', array(), $this->siteOptions + array('backend' => NULL) + $options);
    $parsed = $this->parse_backend_output($this->getOutput());
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=1 name=admin status=1 password=XXXXXXXXXX', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=2 name=arthur status=1 password=XXXXXXXXXX', 'success'));
    // This debug logging was removed because it ate memory.
    //$this->assertTrue($this->assertLogHasMessage($parsed['log'], "Check: uid=3 name=ford for password 'ford'", 'debug'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=3 name=ford status=1 password=XXXXXXXXXX', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=4 name=zaphod status=0 password=XXXXXXXXXX', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=8 name=nimda status=1 password=XXXXXXXXXX', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=2 name=arthur status=1 password=12345', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=5 name=marvin status=0 password=XXXXXXXXXX', 'success'));
  }

  /**
   * Test --no-guessing option.
   */
  public function testDtrNoGuessingOption() {
    $options = array('no-guessing' => NULL);
    $this->drush('drop-the-ripper', array(), $this->siteOptions + array('backend' => NULL) + $options);
    $parsed = $this->parse_backend_output($this->getOutput());
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=2 name=arthur status=1 password=12345', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=3 name=ford status=1 password=abc123', 'success'));
    $this->assertTrue($this->assertLogHasMessage($parsed['log'], 'Match: uid=4 name=zaphod status=0 password=computer', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=1 name=admin status=1 password=admin', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], "Check: uid=3 name=ford for password 'ford'", 'debug'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=5 name=marvin', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=6 name=trillian', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=7 name=slartibartfast', 'success'));
    $this->assertTrue($this->assertLogHasNotMessage($parsed['log'], 'Match: uid=8 name=nimda', 'success'));
  }

}
